1.0.2
* Added possibility to define a load order by prepending "xx_" to the subfolders, where "xx" is a numeric value of any length.

1.0.1
* Automatically load plugins from subfolders without the need to add them to the proxy.

1.0
* Initial version