<?php
/**
 * Plugin Name: Protect links against spam-bots.
 * Plugin URI:  https://rekow.ch
 * Description: Buffers the entire WP process and captures its output in order to obfuscate any mailto: and tel: links.
 * Version:     1.0.4.3
 * Author:      Nils Rekow
 * Author URI:  https://rekow.ch
 * License:     GPL3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

define('OBFUSCATE_ENCODE_AT', false);
define('OBFUSCATE_PROTOCOLS', array('mailto:', 'tel:'));
define('OBFUSCATE_DATA_ATTR', 'data-encrypted');

// Disallow direct access.
if (!defined('ABSPATH')) {
	die();
}


/**
 * Obfuscates a given string and returns the URL-encoded result.
 *
 * @param string $str
 * @return string
 */
function encryptLink($str) {
	$chrCode = 0;
	$result = '';
	
	for ($i = 0; $i < strlen($str); $i++) {
		$chrCode = ord($str[$i]);
		if ($chrCode >= 8364) {
			$chrCode = 128;
		}
		
		$result .= chr($chrCode + 4);
	}
	
	$result = urlencode($result);
	return $result;
}


/**
 * Fallback for Nginx
 */
if ( ! function_exists('apache_request_headers')) {
	function apache_request_headers() {
		$headers = array();
		foreach($_SERVER as $key => $value) {
			if (substr($key, 0, 5) <> 'HTTP_') {
				continue;
			}
			$header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
			$headers[$header] = $value;
		}
		return $headers;
	}
}


// We need to check the headers because is_admin() is - for whatever reason - always false when saving posts, so double-check.
$headers = apache_request_headers();
if (is_admin() !== true && (!isset($headers['Content-Type']) || $headers['Content-Type'] != 'application/json')) {
	// Start output buffering.
	ob_start();
	
	/**
	 * A filter to protect mailto: and tel: links. Also replaces '@' with ' (at) '.
	 */
	add_filter('obfuscate_links', function($content) {
		foreach (OBFUSCATE_PROTOCOLS as $protocol) {
			while (strpos($content, $protocol) !== false) {
				// Find start/end of link by protocol.
				$idx_start = strpos($content, $protocol);
				
				// Only proceed if the link contains the protocol.
				if ($idx_start !== false) {
					$idx_end = strpos($content, '"', $idx_start);
					$quote = '"';
					
					// Extract link from content.
					$link = substr($content, $idx_start, $idx_end - $idx_start);
					
					// Extract address from link (e.g. strip protocol).
					$address = substr($link, strlen($protocol));
					
					// Generate obfuscated link from extracted link.
					$encrypted_link = '#' . $quote . ' ' . OBFUSCATE_DATA_ATTR . '="' . encryptLink($link) . '"';
					
					// Sanitize extracted address.
					if (defined('OBFUSCATE_ENCODE_AT') && OBFUSCATE_ENCODE_AT !== false) {
						$sanitized_address = str_replace('@', '&#x0040;', $address);
					} else {
						$sanitized_address = str_replace('@', ' (at) ', $address);
					}
					
					// Replace contents with generated strings.
					$content = str_replace($link, $encrypted_link, $content);
					$content = str_replace($address, $sanitized_address, $content);
				}
			}
		}
		
		return $content;
	});
	
	
	/**
	 * An action which is triggered on shutdown (e.g. prior outputting the generated content).
	 * Upon shutdown this function collects all buffers and writes their content into one string
	 * It then applies the "obfuscate_links" filter on that string.
	 */
	add_action('shutdown', function() {
		// Get the number of ob-levels, iterate over each and collect that buffer's output.
		$levels = ob_get_level();
		$output = '';
		for ($i = 0; $i < $levels; $i++) {
			$output .= ob_get_clean();
		}
		
		// Apply a filter to the final output.
		echo apply_filters('obfuscate_links', $output);
	}, 0);
	
	
	/**
	 * Register and enqueue some JavaScript in order to be able to decrypt links upon click.
	 */
	add_action('wp_enqueue_scripts', function() {
		$minified = '.min';
		if (defined('WP_DEBUG') && WP_DEBUG !== false) {
			$minified = '';
		}
		
		$filename = 'js/obfuscate-links' . $minified . '.js';
		$timestamp = false;
	
	
		// Prepare locations.
		$file = __DIR__ . DIRECTORY_SEPARATOR . $filename;
		
		// Make sure to use the correct directory separators.
		$file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
		$file = str_replace('/', DIRECTORY_SEPARATOR, $file);
		
		// Check if file exists and get timestamp.
		if (file_exists($file)) {
			$timestamp = filemtime($file);
			wp_register_script('obfuscate-links-js', plugins_url($filename, __FILE__), array('jquery'), $timestamp);
			wp_enqueue_script('obfuscate-links-js');
		} else {
			// Log any errors
			error_log('Cannot enqueue missing file ' . $file);
		}
	});
}