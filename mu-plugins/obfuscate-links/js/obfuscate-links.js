/**
 * Decrypt strings
 * 
 * @param string
 * @returns string
 */
function Decrypt(s, x) {
	var n = 0, r = '';
	
	x = x || false;

	if (x === false) {
		s = decodeURI(s);
	}
	
	for (var i = 0; i < s.length; i++) {
		n = s.charCodeAt(i);
		if (n >= 8364) {
			n = 128;
		}
		
		if (x !== false) {
			r += String.fromCharCode(n + 4);
		} else {
			r += String.fromCharCode(n - 4);
		}
	}
	
	if (x !== false) {
		r = encodeURI(r);
	}
	
	return r;
}


/**
 * Encrypted link click handler.
 */
function ClickAd(s) {
	location.href = Decrypt(s);
	return false;
}

/**
 * jQuery click handler
 */
jQuery(function($) {
	$('a').click(function() {
		let encryptedLink = $(this).data('encrypted');
		if (encryptedLink !== null && encryptedLink !== '' && encryptedLink.length > 0) {
			location.href = Decrypt(encryptedLink);
			return false;
		}
		return true;
	});
});