# Changes
1.0.4.3
* Replaced onclick-attribute with data-attribute.
* Fixed issue when saving a post (e.g. is_admin() is always false when saving a post in the backend. WTF!?).


1.0.4.2
* Fixed link generation


1.0.4.1
* Internal build


1.0.4
* Fixed invalid JSON when editing posts.


1.0.3
* Added minified JS.
* Added fallback to unminified JS if debug-mode is enabled.


1.0.2
* Only proceed if the link contains the protocol.
* Added flag to encode the "@" as "&x0040;" rather than to replace it with " (at) ".


1.0.1
* Disallow plugin to run while in backend.


1.0
* Initial version.