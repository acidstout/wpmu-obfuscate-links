# WPMU Obfuscate Links
This plugin obfuscates all mailto: and tel: links in order to protect your website against spam-bots. Also it replaces the '@' in e-mail-addresses with ' (at) '.

## Installation
Copy the plugin's folder into your wp-content/mu-plugins/ folder. The plugin will then be used automatically by WordPress. I use a proxy plugin to be able to load the plugin from a subfolder. In case you have other mu-plugins you may wish to put them into separate folders. 

## Configuration
There's no configuration at this time. Everything is done automatically.

## Uninstallation
If you use other mu-plugins, just remove the obfuscate-links folder from the mu-plugins folder. Otherwise you likely want to remove the proxy plugin, too.