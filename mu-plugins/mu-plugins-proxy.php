<?php
/**
 * Plugin Name: Simple proxy to load mu-plugins from subfolders.
 * Plugin URI:  https://rekow.ch
 * Description: In order to prevent a mess in the mu-plugins folder this proxy plugin loads plugins from subfolders.
 * Version:     1.0.2
 * Author:      Nils Rekow
 * Author URI:  https://rekow.ch
 * License:     GPL3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

// Disallow direct access.
if (!defined('ABSPATH')) {
    die();
}

// Get subfolder names from mu-plugins directory.
$folders = array_map('basename', glob(__DIR__ . '/*', GLOB_ONLYDIR));

// Sort folder names in order to allow a user-defined loading order. 
natcasesort($folders);

// Iterate over each folder and load the respektiv plugin.
foreach ($folders as $folder) {
    $plugin_file = $folder;
    
    $number = explode('_', $plugin_file);
    if (is_numeric($number[0])) {
        // Cut-off numeric value from plugin filename.
        $plugin_file = substr($plugin_file, strpos($plugin_file, '_'));
    }
    
    $plugin_file = WPMU_PLUGIN_DIR . '/' . $folder . '/' . $plugin_file . '.php';
    if (file_exists($plugin_file)) {
        require_once $plugin_file;
    }
}
